
describe('Login for PPSC',()=>{
    // it('Try to login with an empty text fields', ()=>{
    //     cy.visit('http://103.175.192.45/')
    //     cy.get('.select').select('EN')

    // })
    it('Check the validation of text fields with empty ',()=>{
        cy.visit('http://103.175.192.45/')
        cy.get('.select').select('EN')
        cy.get('input#username').click()
        cy.get('input#password').click()
        cy.get('button.btn').click()
        cy.get('.error').eq(0).should('have.text', '  Username/Email is required')
        cy.get('.error').eq(1).should('have.text', '  Password is required')
    });

    it('With valid input data', ()=>{
        cy.visit('http://103.175.192.45/')
        cy.get('.select').select('EN')
        cy.get('input#username').type('bijay12')
        cy.get('input#password').type('Test@123')
        cy.get('.btn').contains('Submit').click()
        cy.get('.text-black').should('have.text','Dashboard')
        cy.get('.mb-2').should('have.text','Vacancy')
    });

    it('Login with invalid data',()=>{
        cy.visit('http://103.175.192.45/')
        cy.get('.select').select('EN')
        cy.get('input#username').type('bijay120')
        cy.get('input#password').type('Test@123')
        cy.get('.btn').contains('Submit').click()
        cy.get('.toast--message').should('have.text', 'Bad credentials')
    });

})
